import numpy as np
import pandas as pd

def read_raw_measurements(fn, fnext, fnmin, fnmax, key, dfheader=None, dfindex_col=None):
    """
    Read raw KSW measurements, saved by the NI VI application
    :fn: filename without the extension
    :fnext: filename extension, including the dot if needed
    :fnmin: filename number start
    :fnmax: filename number end
    :key: key of the column that will be stored as successive Series in the dataframe
    :dfheader: for csv read, type of header, see read_csv docstring 
    """
    dfret = pd.DataFrame()
    for i in range(fnmin,fnmax):
        ffn = fn + str(i) + fnext
        dfin=pd.read_csv(ffn, sep='\t', header=dfheader, index_col=dfindex_col)
        if i==fnmin:
            # transfer first measurments as the first Series, which index we'll reuse for all the others
            dfret[ffn] = pd.Series(dfin[key])
        else:
            dfret[ffn] = pd.Series(dfin[key], index=dfret.index)
    return dfret
            
